package org.kllbff.mychat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class Server {
    static Logger LOG = Logger.getLogger(Server.class.getName());
    
    public static void main(String[] args) {
        try(ServerSocket serverSocket = new ServerSocket(2004)) {
            LOG.info("Сервер запущен");

            boolean kill = false;
            
            while(!kill) {
                try(Socket clientSocket = serverSocket.accept();
                    InputStream inputStream = clientSocket.getInputStream();
                    OutputStream outputStream = clientSocket.getOutputStream()) {

                    LOG.info("Подключен клиент " + clientSocket.getInetAddress().toString());
                    kill = serveClient(inputStream, outputStream);
                }
            }
            
            LOG.info("Сервер остановлен");
            serverSocket.close();
        } catch(IOException ioe) {
            LOG.severe("Ошибка в работе сервера: " + ioe.getMessage());
        }
    }
            
    public static boolean serveClient(InputStream inputStream, OutputStream outputStream) {
        String request = null;
        
        try {
            int status = inputStream.read();
            if(status < 0) {
                return true;
            }
            
            byte[] buffer = new byte[status];
            inputStream.read(buffer, 0, status);
            
            outputStream.write(status);
            outputStream.write(buffer);
            
            request = new String(buffer);
            
            LOG.info("Запрос обработан: " + request.length() + " символов");
        } catch(IOException ioe) {
            LOG.severe("Ошибка обработки запроса: " + ioe.getMessage());
        }
        
        LOG.info("Получено сообщение: " + request);
        
        return false;
    }
}
