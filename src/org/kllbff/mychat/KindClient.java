package org.kllbff.mychat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class KindClient {
    public static void main(String[] args) {
        try(Socket socket = new Socket("localhost", 2004);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream()) {
            
            sendMessage(outputStream, "Привет!");
            String msg = readMessage(inputStream);
            System.out.println(msg);
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    public static void sendMessage(OutputStream out, String msg) throws IOException {
        byte[] bytes = msg.getBytes();
        out.write(bytes.length);
        out.write(bytes);
        out.flush();
    }
    
    public static String readMessage(InputStream in) throws IOException {
        int status = in.read();
        byte[] bytes = new byte[status];
        in.read(bytes, 0, status);
        return new String(bytes);  
    }
    
    public static void killServer(OutputStream out) throws IOException {
        out.flush();
    }
}
