package org.kllbff.mychat;

import java.io.IOException;
import java.net.Socket;

public class EvilClient {
    public static void main(String[] args) {
        try(Socket socket = new Socket("localhost", 2004)) {
            socket.getOutputStream().flush();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
